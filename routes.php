<?php

$routes  = [
    '/admin'                  => [GET => 'AdminCtl::main'],
    '/admin/menu'             => [GET => 'AdminMenuCtl::main'],
    '/admin/menu/add'         => [GET => 'AdminMenuCtl::edit'],    
    '/admin/menu/edit/(.*)'   => [GET => 'AdminMenuCtl::edit'],       
];

?>
