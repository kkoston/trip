{% require_once('templates/admin/header.php'); %}

<div class="contents">
Menus : <br>
<table>
    <tr>
        <th>Menu name</th>
        <th>CSS class</th>
        <th>CSS id</th>
        <th>Selected item CSS class</th>
        <th>Selected item CSS id</th>
    </tr>
    
{% foreach($menus as $menu): %}

    <tr>
        <td>{{$menu->name}}</td>
        <td>{{$menu->css_class}}</td>
        <td>{{$menu->css_id}}</td>
        <td>{{$menu->selected_item_class}}</td>
        <td>{{$menu->selected_item_id}}</td>
        <td><a href="/trip/admin/menu/edit/{{$menu->id}}">Edit</a></td>
        <td><a href="/trip/admin/menu/delete/{{$menu->id}}">Del</a></td>
    </tr>

{% endforeach %}
</table>

<a href="/admin/menu/add">Add</a>
</div>

{% require_once('templates/admin/footer.php'); %}