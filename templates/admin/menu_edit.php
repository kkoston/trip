<? require_once('templates/admin/header.php'); ?>

<table>
    <tr><td>Menu name</td><td><input value="<?=$menu->name?>" id="menu_name"></td></tr>
    <tr><td>CSS class</td><td><input value="<?=$menu->css_class?>" id="menu_class"></td></tr>
    <tr><td>CSS id</td><td><input value="<?=$menu->css_id?>" id="menu_id"></td></tr>
    <tr><td>Selected CSS class</td><td><input value="<?=$menu->selected_item_class?>" id="selected_item_class"></td></tr>
    <tr><td>Selected CSS id</td><td><input value="<?=$menu->selected_item_id?>" id="selected_item_id"></td></tr>
<table>

<table>
    <tr>
        <th>Item name</th>
        <th>URL</th>        
        <th>CSS class</th>
        <th>CSS id</th>
    </tr>
    
<? foreach($items as $item): ?>

    <tr>
        <td><?=$item->name?></td>
        <td><?=$item->url?></td>        
        <td><?=$item->css_class?></td>
        <td><?=$item->css_id?></td>
        <td><a href="/trip/admin/menu/edititem/<?=$item->id?>">Edit</a></td>
        <td><a href="/trip/admin/menu/deleteitem/<?=$item->id?>">Del</a></td>
    </tr>

<? endforeach ?>
</table>

<a href="/trip/admin/menu/additem">Add menu item</a>

<script>


<? require_once('templates/admin/footer.php'); ?>