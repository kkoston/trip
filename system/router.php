<?php
const GET  = 'GET';
const POST = 'POST';

require 'Request.php';

class Router {
    private static $params = '';
    
    public static function init() {        
        Request::$uri   = $_SERVER['REQUEST_URI'];
        Request::$meth  = $_SERVER['REQUEST_METHOD'];
        
        if (isset($_POST)) {
            Request::$forms = json_decode(json_encode($_POST), FALSE);
        }

        if (substr(Request::$uri, 0, strlen(Config\ROOT_URL)) == Config\ROOT_URL) {
            self::$params = substr(Request::$uri, strlen(Config\ROOT_URL), strlen(Request::$uri));
        }

        self::$params = trim(self::$params, '/');
    }

    private static function is_static() {
        foreach (Config\STATIC_DIRS as $dir) {
            $url_pat = '/^' . preg_quote($dir, '/') . '/';
            if (preg_match($url_pat, self::$params, $matches) === 1) {
                
            }
        }
        
        return false;
    }

    public static function route() {
        if (self::is_static()) {
            return false;
        }
        
        require 'routes.php';
        $found = false;
        
        foreach ($routes as $url_pat => $target) {
            $url_pat = '/^' . str_replace('/', '\/', trim($url_pat, '/')) . '$/';
            if (preg_match($url_pat, self::$params, $matches) === 1) {
                if (count($matches) > 0) {
                    array_shift($matches);
                }
                $found = true;
                call_user_func_array($target[Request::$meth], $matches);
            }
        }

        if (!$found) {
            die('Page not found');
        }
    }
}

?>
