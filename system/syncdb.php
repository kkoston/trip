<?php

require_once 'Database.php';
require_once '../config.php';
$db = new Database($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

function printn($s) {
    echo $s . "\n";
}

function get_tables() {
    global $db;
    
    $ret = [];
    
    $res = $db->fetch_all('show tables');
    foreach ($res as $table) {
        $ret[] = $table[0];
    }
    
    return $ret;
}

function drop_table($table) {
    global $db;
    
    $sql = 'drop table ' . $table;
    printn(' * Dropping table ' . $table);
    printn($sql);
    $db->query($sql);
}

function drop_tables($schema) {
    $tables = get_tables();

    foreach ($tables as $name) {
        if (!array_key_exists($name, $schema)) {
            drop_table($name);
        }
    }
}

function get_table_schema($table) {
    global $db;
    
    $db->set_quiet(true);
    $res = $db->fetch_all('desc ' . $table);
    $db->set_quiet(false);
    
    if ($res == []) {
        return false;
    }
    
    foreach ($res as $field) {
        $ret[$field['Field']] = $field['Type'];
    }
    
    return $ret;
}

function create_table($table, $fields) {
    global $db;
    
    printn(' * Creating new table ' . $table);
    $sql = 'create table ' . $table . ' (id int not null auto_increment, ';
        
    foreach ($fields as $name => $type) {
        
        if (preg_match('/ForeignKey\((.*)\)/', $type, $matches) === 1) {
            $name = fixcase($matches[1]);
            $field = $name . '_id int not null, '.
                     'constraint fk_' . $name . 
                     ' FOREIGN KEY (' . $name . '_id) ' .
                     'references ' . $name . '(id) ON update cascade ON delete cascade, ';
                     
            $sql .= $field;
                
        } else {
            $sql .= $name . ' ' . $type . ', ';
        }
    }
    
    $sql .= 'primary key (id))';
    
    printn($sql);
    $db->query($sql);    
}

function remove_column($table, $column) {
    global $db;  
    
    $sql = 'alter table ' . $table . ' drop column ' . $column;  
    printn($sql);
    $db->query($sql);
}

function remove_columns($table, $fields, $table_schema) {
    foreach ($table_schema as $name => $type) {
        if ($name == 'id') continue;
        
        if (!array_key_exists($name, $fields)) {
            remove_column($table, $name);
        }
    }
}

function add_column($table, $column, $type) {
    global $db;  
    
    $sql = 'alter table ' . $table . ' add column ' . $column . ' ' . $type;  
    printn($sql);
    $db->query($sql);    
}

function add_columns($table, $fields, $table_schema) {
    foreach ($fields as $name => $type) {
        if (!array_key_exists($name, $table_schema)) {
            add_column($table, $name, $type);
        }
    }    
}

function alter_types($table, $fields, $table_schema) {
    
}

function alter_table($table, $fields, $table_schema) {
    printn(' * Altering table ' . $table);
    
    return;
    
    remove_columns($table, $fields, $table_schema);
    add_columns($table, $fields, $table_schema);
    alter_types($table, $fields, $table_schema);
}

function fixcase($table) {
    return strtolower(preg_replace("/(.)([A-Z])/", "$1_$2", $table));
}

function model_exists($model) {
    return file_exists('../models/' . $model . 'Model.php');
}

function create_model($model, $fields) {
    printn(' * Creating model ' . $model . 'Model.php');
    $model_name = $model . 'Model';
    $props = '    public $id = null;' . "\n";
    
    foreach ($fields as $name => $type) {
        $props .= "    public \$" . $name . " = null;\n";
    }
    
    $model = "<?\nclass " . $model_name . " extends Model {\n".$props."}\n?>";
    
    file_put_contents('../models/' . $model_name . '.php', $model);
}

function alter_model($model) {
    printn(' * Altering model ' . $model . 'Model.php');
}

# main script

$yaml = file_get_contents('../models/schema.yaml');
$schema = yaml_parse($yaml);

foreach ($schema as $table => $fields) {
    $fixed[fixcase($table)] = $fields;
}

drop_tables($fixed);

// sync database
foreach ($fixed as $table => $fields) {
    if ( ($table_schema = get_table_schema($table)) === false) {
        create_table($table, $fields);
    } else {
        alter_table($table, $fields, $table_schema);
    }
}

// sync models
foreach ($schema as $model => $fields) {
    if ( !model_exists($model)) {
        create_model($model, $fields);
    } else {
        alter_model($model, $fields);
    }
}

?>