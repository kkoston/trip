<?

class Model {
    public $object_name = null;
    public $table_name = null;
    
    public static function fixcase($name) {
        return strtolower(preg_replace("/(.)([A-Z])/", "$1_$2", $name));     
    }
    
    public static function get_table_name($name) {
        return self::fixcase(substr($name, 0, -5));
    }
    
    public static function parse_conds($conds) {
        $key = '';
        $val = '';
        $in_key = true;
        $in_escape = false;
        $res = [];
        
        for ($i = 0; $i < strlen($conds); $i++) {
            $c = $conds[$i];
            
            if ($c == '\\') {
                if (!$in_escape) {
                    $in_escape = true;
                    continue;   
                }
            }
            
            if ($c == ',' && !$in_escape) {
                $res[trim($key)] = $val;
                $in_key = true;
                $key = '';
                $val = '';
                $in_escape = false;  
                continue;
            }
            
            if ($c == ':' && !$in_escape) {
                $in_key = false;
                $in_escape = false;                
                continue;
            }               
            
            $in_escape = false;
            
            if ($in_key) {
                $key .= $c;
            } else {
                $val .= $c;
            }
            
            if ($i == strlen($conds) - 1) {
                $res[trim($key)] = $val;                
            }
        }
        
        return $res;
    }
    
    public function __construct() {
        $this->object_name = get_class($this);
        $this->table_name = self::get_table_name($this->object_name);
    }
    
    private function update($props) {
        global $db;
        
        $id = $props['id'];
        unset($props['id']);
        
        $fields = array_keys($props);
        $values = array_values($props);
        $setvals = implode('=?, ', $fields) . '=?';
        
        $sql = 'UPDATE ' . $this->table_name . 
               ' SET ' . $setvals .
               ' WHERE id=' . $id;

        $db->query($sql, $values);        
    }
    
    private function insert($props) {
        global $db;
        
        $fields = array_keys($props);
        $values = array_values($props);
        $placeholders = substr(str_repeat('?,', count($values)), 0, -1);
        
        $sql = 'INSERT INTO ' . $this->table_name . 
               ' (' . join(', ', $fields) . ') ' .
               'VALUES ('. $placeholders . ')';

        $db->query($sql, $values);
        
        $this->id = $db->lastInsertId();
    }

    public static function result_to_objects($res, $obj_name) {        
        // fields can not be int
        $ret = [];
        
        foreach($res as $row) {
            $obj = new $obj_name();
            foreach($row as $field => $value) {
                if (is_int($field)) {
                    continue;
                }
                $obj->$field = $value;
            }
            $ret[] = $obj;
        }

        return $ret;
    }
    
    public static function find($conds, $options) {
        global $db;
        
        $conds = self::parse_conds($conds);
        $options = self::parse_conds($options);
        $table_name = self::get_table_name(get_called_class());
            
        $fields = array_keys($conds);
        $values = array_values($conds);
        $getvals = implode('=?, ', $fields) . '=?';
        
        $sql = 'SELECT * FROM ' . $table_name;
        if (!empty($conds)) {
            $sql .= ' WHERE ' . $getvals;
        }
        
        $res = $db->fetch_all($sql, $values);
        
        return self::result_to_objects($res, get_called_class());
    }
    
    public static function aggregate() {
        
    }
    
    public static function annotate() {
        
    }
    
    public static function delete() {
        
    }
    
    public function save() {        
        $fields = '(';
        $props = get_object_vars($this);
        
        // get rid of the superclass (Model) properties
        $parent_name = get_parent_class($this);
        $parent_props = get_class_vars($parent_name);
        foreach ($parent_props as $key => $value) {
            unset($props[$key]);
        }
        
        // take care of references - save if refed objects don't exist
        // and swith the propery value to id for saving
        foreach($props as $field => $value) {
            if (is_object($value)) {
                if (!isset($value->id)) {
                    $this->$field->save();
                }
                $new_value = $this->$field->id;
                $new_field = $value->table_name . '_id';
            } else {
                $new_value = $value;
                $new_field = $field;               
            }
            
            $new_props[$new_field] = $new_value;
        }
        
        $props = $new_props;
        
        if ($props['id'] != null) {
            $this->update($props);
        } else {
            unset($props['id']);
            $this->insert($props);
        }
        
        return $this->id;
        
    }
    
}

?>