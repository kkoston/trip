<?
class TemplateData {
}

class Templates {
    public static function compile($tpl) {
        $hash = md5($tpl);
        $code = file_get_contents($tpl);
        $code = str_replace('{%', '<? ', $code);
        $code = str_replace('%}', ' ?>', $code);
        $code = str_replace('{{', '<?=', $code);
        $code = str_replace('}}', '?>',  $code);
        file_put_contents('templates_c/' . $hash, $code);
        
        return $hash;
    }
    
    public static function render($tpl, $data) {
        $data = (array)$data;
        extract($data);
        $tpl_hash = self::compile('templates/' . $tpl);
        require_once('templates_c/' . $tpl_hash);
    }

    public static function html_tr($data) {
        $result = '<tr>';
        foreach ($data as $cell) {
            $result .= '<td>' .$cell. '</td>';
        }
        $result .= '</tr>';  
    
        echo $result;  
    }

    public static function crud_table($headers, $rows, $class, $id, $crud_url) {
        global $ROOT_URL;
        
        $data = new TemplateData();
        $data->headers = array_merge($headers, ['', '']);
        foreach ($rows as $id => $row) {
        
            $edit = '<a href=' . $ROOT_URL . $crud_url . '/edit/' . $id . '>Edit</a>';
            $del  = '<a href=' . $ROOT_URL . $crud_url . '/delete/' . $id . '>Del</a>';
        
            $data->rows['id'] = array_merge($row,[$edit, $del]);
        }
        $data->class = $class;
        $data->id = $id;

        Templates::render('common/crud_table.php', $data);
    }
}

?>

