<?php
class Database {
    private $quiet = false;
    
    function __construct($db_host, $db_user, $db_pass, $db_name) {
        $this->db_host = $db_host;
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;   
    }
    
    function connect() {
        try {
            $this->db = new PDO(
                'mysql:host='.$this->db_host.';dbname='.$this->db_name.';charset=utf8', 
                $this->db_user, 
                $this->db_pass
            );
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {    
            if (!$this->quiet) echo "DB connect error: " . $e->getMessage() . "<br/>";
        }
    }
    
    public function set_quiet($val) {
        $this->quiet = $val;
    }
    
    public function fetch_all() {
        $args = func_get_args();
        $sql = array_shift($args);
        $ret = [];
        
        if (is_array($args[0])) {
            $args = $args[0];
        }
               
        try {
            $sth = $this->db->prepare($sql);
            $sth->execute($args);
            $ret = $sth->fetchAll();
        } catch (PDOException $e) {
            if (!$this->quiet) echo 'DB fetch_all error: ' . $e->getMessage() . ' ' . $sql;
        }        
        
        return $ret;
        
    }       
    
    public function fetch_one() {
        $args = func_get_args();
        $sql = array_shift($args);
        $ret = [];

        if (is_array($args[0])) {
            $args = $args[0];
        }
        
        try {   
            $sth = $this->db->prepare($sql);
            $sth->execute($args);
            $ret = $sth->fetch();
        } catch (PDOException $e) {    
            if (!$this->quiet) echo 'DB fetch error: ' . $e->getMessage() . ' ' . $sql;;
        }        
           
        return $ret;       
    }
    
    public function query() {
        $args = func_get_args();
        $sql = array_shift($args);
        
        if (is_array($args[0])) {
            $args = $args[0];
        }
        
        try {
            $sth = $this->db->prepare($sql);
            $sth->execute($args); 
        } catch (PDOException $e) {    
            if (!$this->quiet) echo 'DB query error: ' . $e->getMessage() . ' ' . $sql;;
        }       
    }
    
    public function lastInsertId() {
        return $this->db->lastInsertId();
    }    
}

?>