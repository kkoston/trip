<?
class AdminMenuCtl {
    public static function main() {
        $data = new TemplateData();
        
        $menus = MenuModel::find('', '');
        $data->menus = $menus;        
        Templates::render("admin/menu.php", $data);
    }
    
    public static function edit($id = null) {
        $data = new TemplateData();
        
        if ($id != null) {
            $data->menu = MenuModel::find('id: ' . $id, '')[0];    
            $data->items = MenuItemModel::find('menu_id:' . $id, '');
        } else {
            $data->menu = new MenuModel();
            $data->items = [];
        }
               
        Templates::render("admin/menu_edit.php", $data);
    }
}
?>