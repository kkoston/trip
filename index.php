<?php

require_once 'Config.php';
require 'system/Templates.php';
require_once 'system/router.php';

function get_dirs($dir) {
    $path = realpath($dir);
    $res[] = $path;
    $objects = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST
    );
    foreach($objects as $name => $object){
        if (is_dir($name) and basename($name) != '.' and basename($name) != '..') {
            $res[] = $name;
        }
    }
    
    return $res;
}

function autoload_model($className) {
    global $model_dirs;
    
    foreach ($model_dirs as $dir) { 
        $filename = $dir . "/" . $className . ".php";
        if (is_readable($filename)) {
            require $filename;
        }
    }
}
 
function autoload_controller($className) {
    global $controller_dirs;
    
    foreach ($controller_dirs as $dir) { 
        $filename = $dir . "/" . $className . ".php";
        if (is_readable($filename)) {
            require $filename;
        }
    }
}

$controller_dirs = get_dirs('controllers');
$model_dirs = get_dirs('models');
    
spl_autoload_register("autoload_model");
spl_autoload_register("autoload_controller");

require_once 'system/Database.php';
require_once 'system/Model.php';

$db = new Database(
    Config\DB_HOST, 
    Config\DB_USER, 
    Config\DB_PASS, 
    Config\DB_NAME
);

$db->connect();

Router::init();
Router::route();

?>

